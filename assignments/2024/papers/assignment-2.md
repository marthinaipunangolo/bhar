# Assignment

- Course Title: **Trends in Artificial Intelligence and Machine Learning**
- Course Code: **TAI911S**
- Assessment: Second Assignment
- Released on: 25/04/2024
- Due Date: 13/05/2024

# Problem

## Problem Description

Consider the dataset available at this [link](https://www.kaggle.com/datasets/jtiptj/chest-xray-pneumoniacovid19tuberculosis).
It contains normal images of a chest X-ray, as welll as chest X-ray images of tuberculosis, covid-19 and pneumonia.
Your task is to implement various classifiers using the following CNN architectures:

- AlexNet;
- ResNet;
- ZFNet.

You will also analyse and compare the performance of the classifiers.
Note that all your models must be implemented in the **Julia** programming language. 

## Assessment Criteria

We will follow the criteria below to assess the assignment:

- Data preparation and preprocessing. (7%)
- Implementation of the AlexNet classifier. (25%)
- Implementation of the ResNet classifier. (25%)
- Implementation of the ZFNet classifier. (25%)
- Analysis and comparison of the classifiers. (13%)
- Overall solution in **Julia**. (5%)

# Submission Instructions

- This assignment is to be completed by groups of students as already set.
- For each group, a repository should be created on [Gitlab](https://about.gitlab.com).
- The submission date is Monday, May 13 2024, at midnight. Please note that _commits_ after that deadline will not be accepted. Therefore, a submission will be assessed based on the repository's clone at the deadline.
- There will be a mark deduction for each day of delay.
- Each group is expected to present its project after the submission deadline. Each student within a group will have to demonstrate their contribution.
- In the case of plagiarism (groups copying from each other or submissions copied from the Internet), all submissions involved will be awarded the mark 0, and each student will receive a warning.
