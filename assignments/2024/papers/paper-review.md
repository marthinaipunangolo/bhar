# Assignment

- Course Title: **Trends in Artificial Intelligence and Machine Learning**
- Course Code: **TAI911S**
- Assessment: Paper Review
- Released on: 29/05/2024
- Due Date: 10/06/2024

# Problem

## Problem Description

In this assignment, students are tasked to review a research paper assigned to them. Each review should include the following components:

1. summary of the paper, including
    
    * the problem being addressed;
    
    * the main contribution of the work;
    
    * the experimental/theoretical results;

2. The related work (previous research contributions related to the problem);

3. The limitations of the paper.

The list of papers is provided in the **References** section. The order of appearance of each paper represents the number attached to it and, therefore, should be attended to by the student assigned the same number in the file "classlist.pdf" uploaded on MS Teams. The latter is not disclosed to protect each student's information.

## Assessment Criteria

We will follow the criteria below to assess the problem:

- The quality and succinctness of the summary. (35%)
- A vibrant discussion of relevant (reasonable balance between the minimum and maximum numbers of papers) literature. (35%)
- The overall quality of the critique. (30%)

# Submission Instructions

- This assignment is to be completed individually.
- For each submission, a repository should be created on [Gitlab](https://about.gitlab.com).
- Within the repository, the submission should be in the form of a report in **PDF format**.
- The submission date is midnight on Monday, June 10, 2024, at midnight. Please note that _commits_ after that deadline will not be accepted. Therefore, a submission will be assessed based on the repository's clone at the deadline.
- Students who fail to submit will be awarded the mark 0.
- Students who fail to submit their assignment on time will have their marks deducted for every two days of delay.
- In the case of plagiarism (groups copying from each other or submissions copied from the Internet), all submissions involved will be awarded the mark 0, and each student will receive a warning.

## References

<a id="1">[1]</a>
Demmler, Tobias and Tamke, Andreas and Dang, Thao and Haug, Karsten and Mikelsons, Lars
Towards Consistent and Explainable Motion Prediction using Heterogeneous Graph Attention.
ArXiv.

<a id="2">[2]</a>
Tsigos, Konstantinos and Apostodilis, Evlampios and Mezaris, Vasileos.
An Integrated Framework for multi-granular explanation for Video Summarisation. ArXiv.

<a id="3">[3]</a>
Hinnerichs, Tilman and Manhaeve, Robin and Marra,  Giuseppe and Dumancic, Sebastijan.
Towards a fully declarative neuro-symbolic language. ArXiv.

<a id="4">[4]</a>
Zhang, Kejia and Zhang, Lan and Pan, Haiwei and  Yu, Baolong
CTS: A Consistency-Based Medical Image Segmentation Model. ArXiv

<a id="5">[5]</a>
Fu, Yao.
Challenges in Deploying Long-Context Transformers: A Theoretical Peak Performance Analysis. ArXiv.

<a id="6">[6]</a>
Vaca-Rubio, Cristian J. and Blanco, Luis and Pereira, Roberto and Caus, Ma`rius
Kolmogorov-Arnold Networks (KANs) for Time Series Analysis. ArXiv.

<a id="7">[7]</a>
Li, Bingdong and  Di, Zixiang and Lu, Yongfan and Qian, Hong and Wang, Feng and  Ke, Yang Tang and Zhou, Aimin
Expensive Multi-Objective Bayesian Optimization Based on Diffusion Models. ArXiv.

<a id="8">[8]</a>
Xia, Jun and Shi, Yiyu.
Towards Energy-Aware Federated Learning via MARL: A Dual-Selection Approach for Model and Client. ArXiv.

<a id="9">[9]</a>
Ahmadi1, Alireza and Halstead1, Michael and Smitt, Claus and McCool, Chris
BonnBot-I Plus: A Bio-diversity Aware Precise Weed Management Robotic Platform. ArXiv.

<a id="10">[10]</a>
Xiang, Chen and Jinshan, Pan and Jiyang, Lu and Fan, Zhentao and Li, Hao 
Hybrid CNN-Transformer Feature Fusion for Single image Deraining. AAAI 2023.

<a id="11">[11]</a>
Qian, Lihua and Wang, Mingxuan and Liu, Yang and Zhou, Hao
Generalising Downsampling from Regular Data to Graphs. AAAI 2023

<a id="12">[12]</a>
Liu, Bing and Luo, Wei and Li, Gang and Huang, Jing and Yang, Bo.
Do we Need an Encoder-Decoder to Model Dynamical Systems on Networks? IJCAI 2023.

<a id="13">[13]</a>
Meng, Ziqiao and Zhao Peilin and Yu, Yang and King, Irwin
Doubly Stochastic Graph-based Non-autoregressive Reaction Prediction. IJCAI 2023.

<a id="14">[14]</a>
Li, Jiang and Liu Jong
Towards Sharp Analysis for Distributed Learning with Random Features. IJCAI 2023.

<a id="15">[15]</a>
Eiben, Eduard and Ordyniak, Sebastian and Paesani, Giacomo and Szeider, Stefan.
Learning Small Decision Trees with Large Domain. IJCAI 2023.

<a id="16">[16]</a>
Feng, Xu and Yu, Wenjian.
A Fast Adaptive Randomized PCA Algorithm. IJCAI 2023.

<a id="17">[17]</a>
Cui, Chenhang and Ren, Yazhou and Pu, Jingyu and Pu, Xiaorong and He, Lifang.
Deep Multi-View Subspace Clustering With Anchor Graph. IJCAI 2023.

<a id="18">[18]</a>
Ramasinghe, Sameera and Simon, Lucey.
A Learnable RAdial Basis Positional Embedding for Coordinate-MLPs. AAAI 2023.

<a id="19">[19]</a>
Cao, Bowen and Ye, Qichen and Xu, Weyuan and Zou, yuexian. 
FTM: A Frame-Level Timeline Modeling Method for Temporal Graph Representation Learning. AAAAI 2023.

<a id="20">[20]</a>
Guan, Wang and Yuhao, Sun and Sijie, Cheng and Sen, Song
Evolving Connectivity for Recurrent Spiking Neural Networks. NEURIPS 2023.

<a id="21">[21]</a>
Zhiyuan, Yan and Yong, Zhang and Xinhang, Yuan and Siwei, Lyu and Baoyuan, Wu
DeepfakeBench: A Comprehensive Benchmark of Deepfake Detectio. NEURIPS 2023.

<a id="22">[22]</a>
Emaad, Khwaja and Yun S., Son and Aaron, Agaruno and Bo, Huang
CELL-E 2: Translating Proteins to Pictures and Back with a Bidirectional Text-to-Image Transforme. NEURIPS 2023.

<a id="23">[23]</a>
Dan, He and Minh-Quang, Pham and Thanh-Le, Ha and Marco, Turchi
Gradient-based Gradual Pruning for Language-Specific Multilingual Neural Machine Translation.EMNLP 2023

<a id="24">[24]</a>
Liming Wang1 Mark Hasegawa-Johnson and Chang D. Yoo.
A Theory of Unsupervised Speech Recognition. ACL 2023

<a id="25">[25]</a>
Jitesh Jain, Jiachen Li1, MangTik Chiu, Ali Hassani, Nikita Orlov, Humphrey Shi.
OneFormer: One Transformer to Rule Universal Image Segmentation. ICVPR 2023.

<a id="26">[26]</a>
Jiaqi, Chen and Jiachen, Lu and Xiatian, Zhu and Li Zhang
Generative Semantic Segmentation. ICVPR 2023.

<a id="27">[27]</a>
Sheng, Xu and Yanjing, Li and Mingbao, Lin and Peng, Gao and Guodong, Guo5 and Jinhu, Lu and Baochang, Zhang
Q-DETR: An Efficient Low-Bit Quantized Detection Transformer. ICVPR 2023.

<a id="28">[28]</a>
Shubhomoy, Das and Md Rakibul, Islam and Nitthilan Kannappan,  Jayakodi and Janardhan Rao, Doppa
Effectiveness of Tree-based Ensembles for Anomaly Discovery: Insights, Batch and Streaming Active Learning. JAIR Vol 80.

<a id="29">[29]</a>
Wenbo, Zhao and Ling, Fan.
Time-Series Representation Learning via Time-Frequency Fusion Contrasting. Frontiers of Artificial Intelligence 2024.

<a id="30">[30]</a>
Jiyuan, Tan and Jose, Blanchet and Vasilis, Syrgkanis
Consistency of Neural Causal Partial Identification. Arxiv 2024.

<a id="31">[31]</a>
Alvin, Heng and Alexandre H., Thiery and Harold, Soh.
Out-of-Distribution Detection with a Single Unconditional Diffusion Model. ArXiv 2024

<a id="32">[32]</a>
Shao-Yuan Lo and Vishal M. Patel.
Adaptive Batch Normalization Networks for Adversarial Robustness. ArXiv 2024

<a id="33">[33]</a>
G. Welper.
Approximation and Gradient Descent Training with Neural Networks. ArXiv 2024.

<a id="34">[34]</a>
Ermal, Toto and M L Tlachac and Elke, Rundenshinner.
AudiBERT: A Deep Transfer Learning Multimodal Classification Framework for Depression Screening. CIKML'2021

<a id="35">[35]</a>
Woojun, Kim and Youngchul, Sung.
Parameter Sharing with Network Pruning for Scalable Multi-Agent Deep Reinforcement Learning. JAAMAS 2022.

<a id="36">[36]</a>
Katrin Sophie, Bohnsack and Marika,Kaden Julia, Abel and Thomas, Villmann
Unsupervised Machine Learning Methods for Artifact Removal in Electrodermal Activity. EBMC 2021.

<a id="37">[37]</a>
Bougareche, Samia and Zehani, Soraya and Mimi, Malika.
Fashion Images Classification using Machine Learning, Deep Learning and Transfer Learning Models. ISPA 2022.

<a id="38">[38]</a>
Yaoyao Xu and Xinjian Zhao and Xiaozhuang,  Song and Benyou, Wang and Tianshu, Yu
Boosting Protein Language Models with Negative Sample Mining, ArXiv 2024

<a id="39">[39]</a>
Ting, Chen and Saurabh, Saxena and Lala, Li and David J, Fleet and Geoffrey E, Hinton..
Pix2seq: A Language Modeling Framework for Object Detection. ICLR 2022

<a id="40">[40]</a>
Siyu, Shi and Ishaan, Malhi and Kevin, Tran and Andrew Y., Ng and Pranav, Rajpurkar.
Unseen Disease Detection for Deep Learning Interpretation of Chest X-rays. Midl 2021.

	


